#!/usr/bin/env python3

#ejer1

num = input("Por favor ingrese un numero:   ")
num = int (num)


if num %2 == 0:
        print("El numero introducido es par")
else:
        print("El numero introducido es impar")

#ejer2

x = 30
y = 15
suma = x+y

if x == 30:
    print("El valor de X es 30")
elif y ==30:
    print("El valor de Y es 30")
elif suma == 30:
    print("El valor de la suma de X e Y es 30")



#ejer3

num = input("Ingrese un numero del 1 al 7 para saber el dia de la semana:   ")
num = int (num)

if num == 1:
    print("Hoy es lunes")
elif num == 2:
    print("Hoy es martes")
elif num == 3:
    print("Hoy es miercoles")
elif num == 4:
    print("Hoy es jueves")
elif num == 5:
    print("Hoy es viernes")
elif num == 6:
    print("Hoy es sábado")
elif num == 7:
    print("Hoy es domingo")
else:
    print("El numero introducido es erróneo, ingrese un numero del 1 al 7")

#ejer4

unidades = input("Ingrese las unidades consumidas en su factura de la luz:  ")
unidades = float(unidades)

if unidades <=50:
    unidades = unidades*3.5
    print("El precio de su factura sería de: "+str(unidades)+" €")
elif unidades >=50 and unidades <=150:
    unidades = unidades*4
    print("El precio de su factura sería de: "+str(unidades)+" €")
elif unidades > 150 and unidades <=250:
    unidades = unidades*5.20
    print("El precio de su factura sería de: "+str(unidades)+" €")
elif unidades > 50:
    unidades = unidades*6.5
    print("El precio de su factura sería de: "+str(unidades)+" €")

#ejer5

numero = input("Ingrese un numero positivo o negativo:  ")
numero = int(numero)

if numero > 0:
    print("El número introducido es positivo")
elif numero < 0:
    print("El número introducido es negativo")
else:
    print("El número es 0")

#ejer6

cadena1 = 'holamundo'
if cadena1.find('mundo'):
    print("La cadena contiene una subcadena")
else: 
    print("La cadena no contiene una subcadena")

#ejer7

cesta = {	
    'boli':
	{
		'marca' : "Bic",
		'precio'  : "0.75€",
		'referencia'  : "552BIC12"
    },
    'lapiz':
	{
		'marca' : "Pritt",
		'precio' : "1.75€",
		'referencia'  : "567PRI13"
	}
}

for x in cesta:
    print(cesta[x])
    for key,val in cesta[x].items:
        print(key,val)

#ejer8

num = int(input("Dame un numero"))
suma = 0

for a in range(1,num+1):
    suma=num+a
    print(suma)

#ejer9


#ejer10

cadena='pynative'
num_car_del=4

cadena=print(cadena[num_car_del:])
print(cadena)

#ejer11

#ejer12

#ejer13

numero1 = int(input('dame un numero: '))

for i in range(1,numero1+1):
    for j in range(1, i+1):
        print(i, end="")
    print()

#ejer14

numero1 = 555

#ejer15

#ejer16

app_name ='programa.exe'
print(app_name.split(".")[-1])

#ejer17