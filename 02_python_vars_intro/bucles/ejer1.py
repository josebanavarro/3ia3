#!/usr/bin/env python3

#ejer1 bucles

usuarios = ['Joe', 'admin', 'Jon', 'Robert', 'David']

for usuario in usuarios:

    if usuario == 'admin':
        print(usuario+", ¿Le gustaría ver un informe de estado?")
    else:
        print("Hola "+usuario+" gracias por iniciar sesion de nuevo")

#ejer2 bucles

#usuarios=[]

if usuarios:
    for usuario in usuarios:

        if usuario == 'admin':
            print(usuario+", ¿Le gustaría ver un informe de estado?")
        else:
            print("Hola "+usuario+" gracias por iniciar sesion de nuevo")

else:
    print("No hay nadie en esta lista, completala con usuarios!!!")


#ejer3 bucles

current_users = ['Joe', 'admin', 'Jon', 'Robert', 'David']
new_users = ['Iker', 'Joseba','Joe','Robert','Jorge']

for new_user in new_users:
    for current_user in current_users:
        if new_user.lower() == current_user:
            print("Este usuario ya esta en la lista actual")
        else:
            print("El nombre de usuario esta disponible")

def square_sum(numbers):
    resul = 0
    for n in numbers:
        resul += n**2
    return resul


