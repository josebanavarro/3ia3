#!/usr/bin/env python3

#ejercicio1

nombre="joseba"

print("hola "+nombre+", ¿te gustaría aprender algo de Python hoy?")

#ejercicio2

nombre='iker'

print(nombre.upper())

print(nombre.lower())

print(nombre.capitalize())

#ejercicio3

print('Pedro dijo: "Hay que quitarse la corbata para ahorrar energia"')

#ejercicio4

persona_famosa='Pedro'
mensaje=' dijo: "Hay que quitarse la corbata para ahorrar energia"'
print(persona_famosa+mensaje)

#ejercicio5

cadena="\t  \r hola buenos dias \t \r"

print(cadena)
print(cadena.lstrip())
print(cadena.rstrip())
print(cadena.strip())
