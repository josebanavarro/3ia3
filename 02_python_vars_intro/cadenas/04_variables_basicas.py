#!/usr/bin/env python3

#ejercicio1

carname="volvo"

print(carname)

#ejercicio2

x=50

print(x)

#ejercicio3

x=5

y=10

print(x+y)

#ejercicio4

z=x+y

print(z)

#ejercicio5

my_first_name="john"

print(my_first_name)

#ejercicio6

x =y =z = "orange"
print(x)
print(y)
print(z)

#ejercicio7

cars=100
space_in_a_car=5
drivers=30
passengers=50
cars_not_driven=cars-drivers
cars_driven=drivers
carpool_capacity=(cars_driven*space_in_a_car)-drivers
med=carpool_capacity/passengers

print("Hay ",cars," coches disponibles")
print("Solo hay ",drivers," conductores disponibles")
print("Hay ",cars_not_driven," coches vacios disponibles")
print("Podemos transportar",carpool_capacity," personas")
print("Media de pasajeros por coche: "+med)

#ejercicio8

name= "david"

rango=slice(0,1)
rango3=slice(2,3)
rango4=slice(4,5)

subcadena=name[rango]
subcadena3=name[rango3]
subcadena4=name[rango4]

resultado=subcadena+subcadena3+subcadena4

print("La subcadena es: "+resultado)

#ejercicio9

name1="JhonDipPeta"
name2="JaSonAy"

rango1 = slice(4,7)
rango2 = slice (2,5)

subcadena1 = name1[rango1]
subcadena2 = name2[rango2]

print("La subcadena del primer nombre es: "+subcadena1)
print("La subcadena del segundo nombre es: "+subcadena2)

#ejercicio10

name1="japon"
name2="china"

rango=slice(0,1)
rango3=slice(2,3)
rango4=slice(4,5)

subcadena=name1[rango]
subcadena3=name1[rango3]
subcadena4=name1[rango4]

resultado1=subcadena+subcadena3+subcadena4

rango=slice(0,1)
rango3=slice(2,3)
rango4=slice(4,5)

subcadena5=name2[rango]
subcadena6=name2[rango3]
subcadena7=name2[rango4]

resultado2=subcadena5+subcadena6+subcadena7

resultadofinal = resultado1+resultado2

print("El resultado final es:" +resultadofinal)

