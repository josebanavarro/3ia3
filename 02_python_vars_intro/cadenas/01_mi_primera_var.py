#!/usr/bin/env python3


mensaje = "hola mundo!"

print(mensaje)

print(mensaje)

# Phyton es un tipado dinamico, puede cambiar su tipo de valor
# en tiempo de ejecucion 

MyVar = 21
MyVar = "string"

# Obtener el tipo de una variable

print(type(MyVar))

# <clash 'str'>
# str es la simplificación de string

# Asignación multiple
# Mismo valor para diferentes variables

x = y = z = 10
print(x)
print(y)
print(z)
# En una misma linea valores y variables diferentes

a,b,c = 5,"joseba",14.5
print(a)
print(b)
print(c)
