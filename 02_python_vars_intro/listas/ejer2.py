#!/usr/bin/env python3

#ejer1
invitados = ["Jose", "Eva", "Mireia"]
print("Estas invitado a cenar "+invitados[0])
print("Estas invitada a cenar "+invitados[1])
print("Estas invitada a cenar "+invitados[2])

#ejer2

print(invitados[2]+" no puede asistir")
del invitados[2]
invitados.insert(2,"Susana")

print("Estas invitado a cenar "+invitados[0])
print("Estas invitada a cenar "+invitados[1])
print("Estas invitada a cenar "+invitados[2])

#ejer3

print("Hemos encontrado una mesa mas grande para la cena")
invitados.insert(0,"Manu")
invitados.insert(2,"Angel")
invitados.append("Elen")
print("Estas invitado a cenar "+invitados[0])
print("Estas invitado a cenar "+invitados[1])
print("Estas invitado a cenar "+invitados[2])
print("Estas invitada a cenar "+invitados[3])
print("Estas invitada a cenar "+invitados[4])
print("Estas invitada a cenar "+invitados[5])

#ejer4
print("Malas noticias, solo 2 personas podrán venir a cenar")
print(invitados[0]+" ya no estas invitado a cenar ")
invitados.pop(0)
print(invitados[0]+" ya no estas invitado a cenar ")
invitados.pop(0)
print(invitados[0]+" ya no estas invitado a cenar ")
invitados.pop(0)
print(invitados[0]+" ya no estas invitado a cenar ")
invitados.pop(0)


