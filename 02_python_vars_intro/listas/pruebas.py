#!/usr/bin/env python3

lista = ['cero uno dos', 1,2,'nombre']
print(lista[0])
print((lista[0].upper()))
print((lista[0].lower()))
print((lista[0].title()))
print((lista[0].capitalize()))

print(lista[-1])
print(lista[-2])

lista[1] = 'cadena'
print(lista)

lista1 = ['iker', 123]
lista2 = lista + lista1
print(lista2)

#slicing

print(lista[:])
print(lista[0:])

lista = ['cEro tRes' , 1, 2, 'nombre']
print(lista[1:2]) #1
print(lista[0:-1]) #'cero tres, 1 , 2

listanum1 = [1,2,3]
listanum2 = [4,5,6]

print(listanum1*3)

lista_usuarios=['ariane','urko']
lista_usuarios.append('camilo')
print(lista_usuarios)
lista_usuarios.insert(0,'joseba')
print(lista_usuarios)

#del.lista_usuarios[1]
#print(lista_usuarios)

pop_var=lista_usuarios.pop()
print(pop_var)
lista_usuarios.remove('urko')
print(lista_usuarios)

lista_numeros = [1,2,2,3,4,5]
#del.lista_numeros[2]
#print(lista_numeros)

lista_numeros1 = [3,6,2,3,4,8,3]
lista_numeros1.sort()
print(lista_numeros1)
lista_numeros1.reverse()
print(lista_numeros1)

len(lista_numeros1) #7

lista_ejemplo = [[1,2,3],[4,5,6],[7,8,9],[10,11,12]]
len(lista_ejemplo) # len = 4, aunque haya listas dentro de listas, el len te muestra el tamaño de la lista principal


