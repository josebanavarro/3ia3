#!/usr/bin/env python3

nombres = ["Beñat", "Fran", "Kike", "Mikel"]

print(nombres[0])
print(nombres[1])
print(nombres[2])
print(nombres[-1])


print("Buenos dias "+ nombres[0])
print("Buenos dias "+ nombres[1])
print("Buenos dias "+ nombres[2])
print("Buenos dias "+ nombres[-1])

coches = ["Masserati", "Ferrari", "Mercedes"]

print(coches[0]+" es una marca de coche con un diseño elegante y deportivo")
print("Los pilotos de "+coches[1]+" no ganan una carrera en F1 ni aunque compitan solos")
print("Los de "+coches[-1]+" estan a punto de sacar un nuevo coche")