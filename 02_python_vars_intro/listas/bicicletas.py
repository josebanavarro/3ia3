#!/usr/bin/env python3


#Las listas pueden contener diferentes tipos de variables en su interior

bicicletas = [1,2,"BH", "Orbea", True]

print(bicicletas) # [1, 2, 'BH', 'Orbea', True]
print(bicicletas[3]) # Orbea
print(bicicletas[-1]) # True
print(bicicletas[:-1]) # [1, 2, 'BH', 'Orbea']
print(bicicletas[1:2]) # [2]