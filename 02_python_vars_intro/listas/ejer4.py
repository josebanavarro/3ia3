#!/usr/bin/env python3

pizzas = ['pepperoni', 'barbacoa', '4 quesos']

for pizza in pizzas:
    print("Me gusta la pizza: "+pizza.title())
    print("Realmente amo la pizza: "+pizza.title()+" voy a pedirme una ahora")

animales = ('perro', 'gato', 'loro')

for animal in animales:
    print("Un "+animal+" sería una gran mascota")
    print("Realmente un "+animal+" seria una gran mascota")