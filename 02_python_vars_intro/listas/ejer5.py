#!/usr/bin/env python3

for value in range(1,21):
    print(value)

for value in range(1,1000001,1):
    print(value)

lista_numeros = list(range(1,1000001))
print(min(lista_numeros))
print(max(lista_numeros))
print(sum(lista_numeros))

numeros_impares = range (1,21,2)
for impares in numeros_impares
    print(numeros_impares)

multiplos_tres = range(0,31,3)
for tres in multiplos_tres:
    print(tres)

numero_cubo = range(0,11)
for cubo in numero_cubo
    print(cubo**3)
