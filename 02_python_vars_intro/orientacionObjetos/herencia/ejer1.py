#!/usr/bin/env python3

class Persona:   
    def __init__(self,nombre,edad,dni):
         self.__nombre = nombre
         self.__edad = edad
         self.__dni = dni   

    @property #propiedad GETTER
    def nombre(self):
        return self.__nombre

    @property #propiedad GETTER
    def edad(self):
        return self.__edad
    
    @property #propiedad GETTER
    def dni(self):
        return self.__dni

    @nombre.setter #Propiedad SETTER
    def nombre(self, nuevo):
        print ("Modificando nombre..")
        self.__nombre = nuevo
        print ("El nombre se ha modificado por")
        print (self.__nombre)

    @edad.setter #Propiedad SETTER
    def nombre(self, nuevo):
        print ("Modificando edad..")
        self.__edad = nuevo
        print ("La nueva edad se ha modificado por")
        print (self.__edad)

    @dni.setter #Propiedad SETTER
    def dni(self, nuevo):
        print ("Modificando dni..")
        self.__dni = nuevo
        print ("La nueva dni se ha modificado por")
        print (self.__dni)

    def mostrarDatos(self):
      datos = self.__nombre + ' ' + str(self.__edad) + ' ' + str(self.__dni)
        return datos 
        
    def esMayorDeEdad(self):
      if self.__edad < 18:
        return 'Eres menor de edad!!!'
      else:
        return 'Eres mayor de edad'

persona1 = Persona('Joseba',18,1111)
persona1.nombre = 'Jose'
print(persona1.mostrarDatos())
print(persona1.esMayorDeEdad())
persona1.edad = 15
ptinr(persona1.esMayorDeEdad())
