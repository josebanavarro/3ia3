#!/usr/bin/env python3


def make_album(nombre='ACDC',titulo='Back in Black',pistas=0):
    artista = {'nombre':nombre, 'album':titulo}
    if(pistas!=0):
        artista['pistas']=pistas
    return artista

print(make_album('Bisbal','Buleria',15))