#!/usr/bin/env python3

def hola_mundo(nombre):
    print("Hola programador "+nombre+"!")

hola_mundo('Joseba')

#ejer1

def display_message():
    print("Estamos aprendiendo a programar")

display_message()

#ejer2

def libro_favorito(titulo):
    print("Uno de mis libros favoritos es "+titulo)

libro_favorito('Harry Potter')