#!/usr/bin/env python3

#8.1

def make_shirt(talla='M', mensaje='Mensaje por defecto'):
    
    """INDICANOS TU TALLA, Y EL MENSAJE A IMPRIMIR EN ELLA"""
    print("\nMi talla es: " + talla)
    print()
    print("Mensaje: "+mensaje)

make_shirt()
make_shirt(talla='L',mensaje='Esta es mi nueva camiseta')

#8.2

def make_shirt(talla='S', mensaje='Me encanta Python!!!!!'):

    """INDICANOS TU TALLA, Y EL MENSAJE A IMPRIMIR EN ELLA"""
    print("\nMi talla es: " + talla)
    print()
    print("Mensaje: "+mensaje)

make_shirt()
make_shirt(talla='L', mensaje='Esta es la camiseta grande')
make_shirt(talla='M', mensaje='Esta es la camiseta mediana')
make_shirt(talla='XL', mensaje='Esta es la camiseta mas tocha que tenemos!!!!')

#8.3

def describe_city(nombre,pais="EH"):

    print(nombre+" está en "+pais)

describe_city('Reikiavic')
describe_city('Reikiavic',pais='Islandia')
describe_city(nombre='Moscú',pais='Rusia')
describe_city(nombre='Gibraltar',pais='España')