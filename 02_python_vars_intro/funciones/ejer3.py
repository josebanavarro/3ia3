#!/usr/bin/env python3

# Pasando un número variable de argumentos

def make_sandwitch(*ingredientes):

    print("\nLos ingredientes de tu sandwitch:")
    for ingrediente in ingredientes:
        print(ingrediente)

make_sandwitch()
make_sandwitch('jamon','queso')
make_sandwitch('lomo','queso','lechuga')


def make_car(fabricante,modelo,*opcionales):
    car={'fabricante':fabricante,'modelo': modelo}
    for key,value in opcionales.items():
        car[key]=value
    print[car]
    
make_car = make_car('renault','megane')
make_car = make_car('volswagen','siroco',color='carbono')
make_car = make_car('citroen', 'c4', color='azul', aleron='negro')
