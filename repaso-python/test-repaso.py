#!/usr/bin/env python3

#-------------------------
# DEFINICIÓN DE VARIABLES
#-------------------------

# Sacar un mensaje por pantalla
print("Hola mundo!")

# Si no le pasas nada a print mete una linea en blanco
print()
print()

print("Fin")

# Definición de variables básica

mensaje = "Joseba"
print(mensaje)

numero = 4
mensaje = "Camilo"
print(mensaje)

# Dar valores a variables en una misma linea
x,y,z = 1,2,3
# Es lo mismo que esto
x = 1
y = 2
z = 3
# Igualas todas las variables al mismo valor

# Le damos a 3 variables el mismo valor
x = y = z = 12

# Numeros
numero = 12
print(type(numero)) # type te dice de que tipo es la variable
print(isinstance(numero,int)) # isinstance te dice con true o false, si es o no el tipo de varible que le has metido

# Operaciones matemáticas básicas
print(2+2) # suma
print(2-1) # resta
print(2*2) # multi
print(2/2) # division
print(5%2) # te devuelve el resto
print(5//2) # te devuelve el resultado entero (sin la parte decimal)
print(2**2)

# STRING = STR = Cadenas de texto
cadena1 = "hola"
cadena2 = "adios"
cadenatotal = cadena1+cadena2 # podemos sumas 2 cadenas (se refiere a concatenarlas o que salgan una seguida de la otra)
print(cadenatotal)
print(cadena1*3) # hacemos que salga 3 veces la misma cadena

nombre = "Joseba Navarro"
print(nombre.title()) # Joseba Navarro - pone la primera letra de cada palabra en mayuscula
print(nombre.capitalize()) # Joseba navarro - pone la primera letra de la primera palabra en mayuscula y el resto en minuscula
print(nombre.upper()) # JOSEBA NAVARRO - pone toda la cadena en mayuscula
print(nombre.lower()) # joseba navarro - pone toda la cadena en minuscula

cadena = " hola "
print(len(cadena.strip())) # La funcion strip elimina espacios por delante y por detras, y con len, te devuelve la longitud de la cadena
print(len(cadena.rstrip())) # La funcion rstrip elimina espacios en blanco solo por la derecha
print(len(cadena.strip())) # La funcion lstrip elimina espacios en blanco solo por la izquierda
linea = ("aaaa;bbbb;cccc")
print(linea.split(';')) # coge la cadena y la divide en 3 subcadenas, y te devuelve una lista de las subcadenas

# Indexaccion o indexar = acceder a un caracter determinado en una posicion determinada

# Indexación
cadena = "TRES"
print(cadena[0])
print(cadena[1])
print(cadena[2])
print(cadena[3])

# Inmutabilidad ~ ERROR no se puede cambiar un caracter individual de una cadena
# cadena[1] = "I"

# Slicing - Significa coger un cacho de la cadena
cadena = "cinco"
# Hacen lo mismo
print(cadena)
print(cadena[:])
print(cadena[0:4])



