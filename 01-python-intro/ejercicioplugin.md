
## PLUGIN BETTER COMMENTS

La extensión Better Comments es ligeramente diferente a las demás. Se enfoca únicamente en hacer comentarios más amigables y legibles para tu código Python. Con esta extensión, puedes organizar tus anotaciones y mejorar la claridad del código. Puedes usar varias categorías y colores para categorizar tus anotaciones-por ejemplo, Alertas, Consultas, TODOs y Highlights. 

También puedes marcar el código comentado con un estilo diferente y establecer varias configuraciones para los comentarios según la forma que desees. Esta extensión gratuita también es compatible con muchos otros lenguajes de programación. 

URL: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments&ssr=false#overview

Installation: Ctrl + P, and write this command: <strong>ext install aaron-bond.better-comments</strong>

### PRINCIPALES CARACTERÍSTICAS:

-   Clasificaciones adicionales de comentarios. Importante, Pregunta, Tarea y Cruzado.
-   Primer plano personalizable para cada clasificación de comentarios.
-   Personalice la configuración de la fuente y la opacidad de sus comentarios.
-   Funciona con C#, F#, VB.NET, C/C++, JavaScript, Python, HTML y XAML.

#### Ejemplo de uso: 

![](better-comments.png)

```
# ? Para preguntas
# ! Para casos Importantes
# TODO para tareas pendientes

```

### Clasificación de los comentarios
-   Utilice '!' para Importante.
-   Utilice '?' para Pregunta.
-   Utilice "Todo" (Caso ignorado) para Tarea.
-   Utilice 'x', 'X' o doble comentario para tachar (tachado).

### Consclusión: 